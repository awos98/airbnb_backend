from django.db import models


class CommonModel(models.Model):
    """Common Model Definition"""

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # Django 에서 model을 configure 할 때 사용
    class Meta:
        # 장고가 해당 모델을 봐도 데이터베이스에 저장을 하지 않음
        abstract = True
